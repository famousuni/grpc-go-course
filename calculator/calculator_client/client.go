package main

import (
	"bufio"
	"context"
	"fmt"
	"grpc-go-course/calculator/calculatorpb"
	"io"
	"log"
	"os"
	"time"

	"google.golang.org/grpc"
)

func main() {
	fmt.Println("CalculatorService Client")
	//create connection to grpc server, disable ssl
	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect: %v", err)
	}
	defer cc.Close()
	//pass connection into client
	c := calculatorpb.NewCalculatorServiceClient(cc)
	//reader code
	printAPI()
	input := bufio.NewScanner(os.Stdin)
	for input.Scan() {
		char := input.Text()
		if char == "1" {
			doUnary(c)
			fmt.Println()
			printAPI()
		} else if char == "2" {
			doServerStreaming(c)
			fmt.Println()
			printAPI()
		} else if char == "3" {
			doClientStreaming(c)
			fmt.Println()
			printAPI()
		} else if char == "4" {
			doBiDiStreaming(c)
			fmt.Println()
			printAPI()
		} else if char == "5" {
			break
		} else {
			fmt.Println("Invalid Choice")
			fmt.Println()
			printAPI()
		}
	}

}

func doUnary(c calculatorpb.CalculatorServiceClient) {
	fmt.Println("Starting to do a Unary RPC...")
	req := &calculatorpb.SumRequest{
		Num_1: 1,
		Num_2: 2,
	}
	//invoke Sum function from pb
	res, err := c.Sum(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling Sum RPC: %v", err)
	}
	log.Printf("Response from Sum: %v", res.Result)
}

func doServerStreaming(c calculatorpb.CalculatorServiceClient) {
	fmt.Println("Starting to do a Server Streaming RPC...")
	req := &calculatorpb.PrimeNumberDecompositionRequest{
		Num_1: 120,
	}
	//invoke PrimeNumberDecomposition function from pb
	resStream, err := c.PrimeNumberDecomposition(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling PrimeNumberDecomposition RPC: %v", err)
	}
	for {
		msg, err := resStream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Error while reading stream: %v", err)
		}
		log.Printf("Response from PrimeNumberDecomposition: %v", msg.GetResult())
	}
}

func doClientStreaming(c calculatorpb.CalculatorServiceClient) {
	fmt.Println("Starting to do a Client Streaming RPC...")
	numbers := []int32{3, 5, 7, 9, 12}
	//dont pass in a request since this is a stream
	stream, err := c.ComputeAverage(context.Background())
	if err != nil {
		log.Fatalf("Error while calling LongGreet RPC: %v", err)
	}
	//iterate over slice and send each msg individually
	for _, num := range numbers {
		log.Printf("Sending req: %v\n", num)
		stream.Send(&calculatorpb.ComputeAverageRequest{
			Num_1: num,
		})
		time.Sleep(1000 * time.Millisecond)
	}
	//send EOF to server to close stream and get server response
	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("Error while recieving response from ComputeAverage: %v,", err)
	}
	log.Printf("ComputeAverage Response: %v\n", res)
}

func doBiDiStreaming(c calculatorpb.CalculatorServiceClient) {
	fmt.Println("Starting to do a BiDi Streaming RPC...")
	//create a stream by invoking client
	stream, err := c.FindMaximum(context.Background())
	if err != nil {
		log.Fatalf("Error while creating stream: %v", err)
		return
	}
	//data to send
	nums := []int32{1, 23, 10, 94, 3}
	//create channel
	waitc := make(chan struct{})
	// go routine to send data
	go func() {
		for _, num := range nums {
			stream.Send(&calculatorpb.FindMaximumRequest{
				Num_1: num,
			})
			time.Sleep(1000 * time.Millisecond)
		}
		// done sending data - server will know when to send EOF after processing
		// this data due to CloseSend
		stream.CloseSend()
	}()
	// go routine to receive data
	go func() {
		for {
			res, err := stream.Recv()
			if err == io.EOF {
				break // all data received
			}
			if err != nil {
				log.Fatalf("Error while receiving: %v", err)
				break
			}
			fmt.Printf("FindMaximum Response: %v\n", res.GetResult())
		}
		// close channel after all data received or after receive error
		close(waitc)
	}()
	//block until channel is closed
	<-waitc
}

//printAPI prints choices
func printAPI() {
	fmt.Println("GRPC Client APIs")
	fmt.Println("----------------")
	fmt.Println("1. Unary")
	fmt.Println("2. Server Streaming")
	fmt.Println("3. Client Streaming")
	fmt.Println("4. BiDi Streaming")
	fmt.Println("5. Quit")
	fmt.Print("-> ")
}
