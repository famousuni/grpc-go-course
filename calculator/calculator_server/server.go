package main

import (
	"context"
	"fmt"
	"grpc-go-course/calculator/calculatorpb"
	"io"
	"log"
	"net"
	"time"

	"google.golang.org/grpc"
)

type server struct{}

func (s *server) Sum(ctx context.Context, req *calculatorpb.SumRequest) (*calculatorpb.SumResponse, error) {
	log.Printf("Sum function was invoked with %v\n", req)
	sum := req.GetNum_1() + req.GetNum_2()
	res := &calculatorpb.SumResponse{
		Result: sum,
	}
	return res, nil
}

func (s *server) PrimeNumberDecomposition(req *calculatorpb.PrimeNumberDecompositionRequest, stream calculatorpb.CalculatorService_PrimeNumberDecompositionServer) error {
	log.Printf("PrimeNumberDecomposition function invoked with %v", req)
	cnum := req.GetNum_1()
	div := int32(2)
	for cnum > 1 {
		if cnum%div == 0 {
			log.Printf("Factor: %v\n", div)
			stream.Send(&calculatorpb.PrimeNumberDecompositionResponse{
				Result: div,
			})
			time.Sleep(1000 * time.Millisecond)
			cnum = cnum / div
		} else {
			div++
			log.Printf("Divisor has increased to %v\n", div)
		}
	}
	return nil
}

func (s *server) ComputeAverage(stream calculatorpb.CalculatorService_ComputeAverageServer) error {
	log.Printf("ComputeAverage function was invoked with a streaming request")
	sum := int32(0)
	count := 0
	for {
		req, err := stream.Recv()
		if err == io.EOF { // end of data
			// average values
			result := float64(sum) / float64(count)
			log.Printf("ComputerAverage Sending: %v", result)
			return stream.SendAndClose(&calculatorpb.ComputeAverageResponse{
				Result: result,
			})
		}
		if err != nil {
			log.Fatalf("Error while reading client stream: %v", err)
		}
		log.Printf("ComputeAverage Client sent: %v", req)
		sum += req.GetNum_1()
		count++
	}

}

func (s *server) FindMaximum(stream calculatorpb.CalculatorService_FindMaximumServer) error {
	log.Printf("FindMaximum function was invoked with a streaming request")
	max := int32(0)
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			//end of data stream
			return nil
		}
		if err != nil {
			log.Fatalf("Error while reading client stream: %v", err)
			return err
		}
		log.Printf("Client sent: %v", req)
		num := req.GetNum_1()
		if num > max {
			max = num
			log.Printf("Current Max Value: %v", max)
			//send only if new max
			err = stream.Send(&calculatorpb.FindMaximumResponse{
				Result: max,
			})
			if err != nil {
				log.Fatalf("Error while sending data to client: %v", err)
				return err
			}
		}
	}

}

func main() {
	fmt.Println("CalculatorService Server")
	//create listener on default grpc port
	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}
	//grpc server
	s := grpc.NewServer()
	calculatorpb.RegisterCalculatorServiceServer(s, &server{})
	// pass in listener
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Faield to serve: %v", err)
	}
}
