package main

import (
	"bufio"
	"context"
	"fmt"
	"grpc-go-course/greet/greetpb"
	"io"
	"log"
	"os"
	"time"

	"google.golang.org/grpc"
)

func main() {
	fmt.Println("GreetService Client")
	//create connection to grpc server, disable ssl
	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect: %v", err)
	}
	defer cc.Close()
	//pass connection into client
	c := greetpb.NewGreetServiceClient(cc)
	//reader code
	printAPI()
	input := bufio.NewScanner(os.Stdin)
	for input.Scan() {
		char := input.Text()
		if char == "1" {
			doUnary(c)
			fmt.Println()
			printAPI()
		} else if char == "2" {
			doServerStreaming(c)
			fmt.Println()
			printAPI()
		} else if char == "3" {
			doClientStreaming(c)
			fmt.Println()
			printAPI()
		} else if char == "4" {
			doBiDiStreaming(c)
			fmt.Println()
			printAPI()
		} else if char == "5" {
			break
		} else {
			fmt.Println("Invalid Choice")
			fmt.Println()
			printAPI()
		}
	}
}

func doUnary(c greetpb.GreetServiceClient) {
	fmt.Println("Starting to do a Unary RPC...")
	req := &greetpb.GreetRequest{
		Greeting: &greetpb.Greeting{
			FirstName: "John",
			LastName:  "Smith",
		},
	}
	//invoke greet function from pb
	res, err := c.Greet(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling Greet RPC: %v", err)
	}
	log.Printf("Response from Greet: %v", res.Result)
}

func doServerStreaming(c greetpb.GreetServiceClient) {
	fmt.Println("Starting to do a Server Streaming RPC...")
	req := &greetpb.GreetManyTimesRequest{
		Greeting: &greetpb.Greeting{
			FirstName: "John",
			LastName:  "Smith",
		},
	}
	//invoke greetmanytimes function from pb
	resStream, err := c.GreetManyTimes(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling GreetManyTimes RPC: %v", err)
	}
	//for loop for recieving data from the stream. The end of the stream
	//will be io.EOF
	for {
		msg, err := resStream.Recv()
		if err == io.EOF {
			//we've reached the end of the stream
			break
		}
		if err != nil {
			log.Fatalf("Error while reading stream: %v", err)
		}
		//log results from stream on each iteration
		log.Printf("Response from GreetManyTimes Server : %v", msg.GetResult())
	}

}

func doClientStreaming(c greetpb.GreetServiceClient) {
	fmt.Println("Starting to do a Client Streaming RPC...")
	requests := []*greetpb.LongGreetRequest{
		&greetpb.LongGreetRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "John",
			},
		},
		&greetpb.LongGreetRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "Charlie",
			},
		},
		&greetpb.LongGreetRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "Toni",
			},
		},
	}
	//dont pass in a request since this is a stream
	stream, err := c.LongGreet(context.Background())
	if err != nil {
		log.Fatalf("Error while calling LongGreet RPC: %v", err)
	}
	//iterate over slice and send each msg individually
	for _, req := range requests {
		log.Printf("Sending req: %v\n", req)
		stream.Send(req)
		time.Sleep(1000 * time.Millisecond)
	}
	//send EOF to server to close stream and get server response
	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("Error while recieving response from LongGreet: %v,", err)
	}
	log.Printf("LongGreet Response: %v\n", res)
}

func doBiDiStreaming(c greetpb.GreetServiceClient) {
	fmt.Println("Starting to do a BiDi Streaming RPC...")
	//create a stream by invoking client
	stream, err := c.GreetEveryone(context.Background())
	if err != nil {
		log.Fatalf("Error while creating stream: %v", err)
		return
	}
	//data to send
	requests := []*greetpb.GreetEveryoneRequest{
		&greetpb.GreetEveryoneRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "John",
			},
		},
		&greetpb.GreetEveryoneRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "Charlie",
			},
		},
		&greetpb.GreetEveryoneRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "Toni",
			},
		},
	}
	//create channel
	waitc := make(chan struct{})
	//send messages to the server (go routine)
	go func() {
		// function to send messages
		for _, req := range requests {
			fmt.Printf("Sending message: %v\n", req)
			stream.Send(req)
			time.Sleep(1000 * time.Millisecond)
		}
		//We are done sending data on the stream
		stream.CloseSend()
	}()
	//recieve messages from the server (go routine)
	go func() {
		// function to receive messages
		for {
			res, err := stream.Recv()
			if err == io.EOF {
				// since all data is recieved close channel/unblock
				break
			}
			if err != nil {
				log.Fatalf("Error while receiving: %v", err)
				break
			}
			fmt.Printf("Received: %v\n", res.GetResult())
		}
		close(waitc)
	}()
	//block until everything is done, wait for channel to be closed
	<-waitc
}

//printApi prints choices
func printAPI() {
	fmt.Println("GRPC Client APIs")
	fmt.Println("----------------")
	fmt.Println("1. Unary")
	fmt.Println("2. Server Streaming")
	fmt.Println("3. Client Streaming")
	fmt.Println("4. BiDi Streaming")
	fmt.Println("5. Quit")
	fmt.Print("-> ")
}
